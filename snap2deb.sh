#!/bin/bash

current_pwd="$PWD"

if [[ -z "$snap2deb_maintainer_name" ]]; then
    snap2deb_maintainer_name="Julian Fairfax"
fi
if [[ -z "$snap2deb_maintainer_email" ]]; then
    snap2deb_maintainer_email="juliannfairfax@protonmail.com"
fi

if [[ -z "$(which snap)" ]]; then
    export DEBIAN_FRONTEND=noninteractive

    sudo apt install snapd -y
fi

if [[ "$3" == "--"* ]]; then
    parameter_3="${3#\-\-}"
else
    parameter_3="$3"
fi

if [[ "$1" == "download" || "$1" == "install" ]]; then
	if [[ ! -z "$2" ]]; then
		snap2deb_snap_id="$(snap info "$2" | grep snap-id | sed 's/.*\:\ //')"
		
		if [[ ! -z "$parameter_3" ]]; then
			snap2deb_snap_build_number="$(snap info "$2" | grep latest/"$parameter_3" | sed 's/.*(//' | sed 's/).*//')"
		else
			snap2deb_snap_build_number="$(snap info "$2" | grep latest/stable | sed 's/.*(//' | sed 's/).*//')"
		fi

		snap2deb_snap_id="${snap2deb_snap_id}_${snap2deb_snap_build_number}"
	fi
fi

cd /tmp

wget -O "$snap2deb_snap_id".snap https://api.snapcraft.io/api/v1/snaps/download/"$snap2deb_snap_id".snap
unsquashfs -q -f -d source "$snap2deb_snap_id".snap

name="$(cat source/meta/snap.yaml | grep name | sed 's/.*\:\ //' 
$name)"
version="$(cat source/meta/snap.yaml | grep version | sed 's/.*\:\ //')"
description="$(cat source/meta/snap.yaml | grep description | sed 's/.*\:\ //')"

install -d $name/opt/$name
cp -r source/. $name/opt/$name

sed -i "s|\${\SNAP}/meta/gui/icon.png|$name|g" $name/opt/$name/meta/gui/$name.desktop
install -Dm644 $name/opt/$name/meta/gui/$name.desktop -t $name/usr/share/applications
install -Dm644 $name/opt/$name/meta/gui/icon.png $name/usr/share/pixmaps/$name.png

rm -rf $name/opt/$name/{data-dir,gnome-platform,lib,meta,scripts,usr,*.sh}

install -d $name/usr/bin
ln -s /opt/$name/$name $name/usr/bin

mkdir $name/DEBIAN

echo "Package: $name
Version: $version
Architecture: $(dpkg --print-architecture)
Maintainer: $snap2deb_maintainer_name <$snap2deb_maintainer_email>
Description: $description" | tee $name/DEBIAN/control

dpkg-deb -Z xz -b $name/ .

rm -r "$snap2deb_snap_id".snap source $name

if [[ "$1" == "install" ]]; then
    export DEBIAN_FRONTEND=noninteractive

    sudo apt install ./${name}_${version}\_$(dpkg --print-architecture).deb && rm ${name}_${version}\_a*.deb
else
    cp ${name}_${version}\_a*.deb $current_pwd && rm ${name}_${version}\_a*.deb
fi
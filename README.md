# snap2deb
snap2deb is a Linux command line tool to convert packages from the snap format to the Debian package format

## Supported Operating Systems
- Debian-based Linux distributions

## Supported snaps
In theory all user snaps, that contain a single binary sharing the snap's name, in the root directory should be supported but server snaps, that have multiple binaries not sharing the snap's name, in multiple directories are not currently supported. If the source code for a snap is available, it's recommend to build a deb yourself but snap2deb is perfect for closed source software! 😉

## Installation
snap2deb can be manually download and run from the GitLab releases page or downloaded and installed from the GitLab CI page.

You can also download it from the Debian repository that is now available [here](https://gitlab.com/julianfairfax/package-repo).

## Usage
snap2deb supports downloading a snap, converting it, and copying it to the current working directory using the `download` option or installing it using the `install` option and requires snapd to be installed. The script will install snapd automatically if it is not installed.

### Step 1

Download the latest version of snap2deb from the GitLab releases page.

### Step 2

Unzip the download and open Terminal. Type `chmod +x` and drag the `snap2deb.sh` script to Terminal. Enter `download` to download, convert, and copy the snap to the current working directory or `install` to install the snap, then the name of your snap, the channel if necessary, and hit enter.

### Make sure to replace these values before running any commands:
- `$maintainer_name` should be replaced with your email by running `export maintainer_name="<NAME>"` or replacing it yourself. If none is set, `Julian Fairfax` will be used.
- `$maintainer_email` should be replaced with your email by running `export maintainer_email="<EMAIL>"` or replacing it yourself. If none is set, `juliannfairfax@protonmail.com`  will be used.

## Building
If you want to build snap2deb you can do so by running the `build.sh` script.

### Make sure to replace these values before running any commands:
- `$maintainer_name` should be replaced with your email by running `export maintainer_name="<NAME>"` or replacing it yourself. If none is set, `Julian Fairfax` will be used.
- `$maintainer_email` should be replaced with your email by running `export maintainer_email="<EMAIL>"` or replacing it yourself. If none is set, `juliannfairfax@protonmail.com`  will be used.

## Docs
If you want to know how snap2deb works, you can check [this doc](https://julianfairfax.gitlab.io/documentation/converting-snaps-to-another-format.html) I wrote about it. You can also review the code yourself and feel free to submit a pull request if you think part of it can be improved.